package ru.t1.avfilippov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.marker.DataCategory;

@Category(DataCategory.class)
public final class UserRepositoryTest {
/*
    @NotNull
    private  final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connectionService.getConnection());


    @NotNull
    private final IUserRepository userRepository = new UserRepository(connectionService.getConnection());

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    @NotNull
    private User userTesting;

    @Before
    public void before() throws AbstractException {
        @NotNull final User user = new User();
        user.setLogin(LOGIN_TEST);
        user.setPasswordHash(HashUtil.salt(propertyService, PASS_TEST));
        user.setRole(Role.USUAL);
        userTesting = userRepository.add(user);
    }

    @After
    public void after() {
        userRepository.clear();
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final User user1 = new User();
        user1.setLogin(LOGIN);
        user1.setPasswordHash(HashUtil.salt(propertyService, PASSWORD));
        user1.setRole(Role.USUAL);
        userRepository.add(user1);
        @Nullable final User user = userRepository.findByLogin(LOGIN);
        Assert.assertNotNull(user);
        projectRepository.add(user.getId(),USER_PROJECT1);
        Assert.assertNotNull(projectRepository.findOneById(USER_PROJECT1.getId()));
    }

    @Test
    public void findByLogin() throws LoginEmptyException {
        Assert.assertNotNull(userRepository.findByLogin(LOGIN_TEST));
    }


    @Test
    public void removeById() throws AbstractFieldException {
        userRepository.removeById(userTesting.getId());
        Assert.assertNull(userRepository.findByLogin(LOGIN_TEST));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist(LOGIN_TEST));
    }
*/
}
