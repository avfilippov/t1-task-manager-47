package ru.t1.avfilippov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.enumerated.Role;

import javax.persistence.EntityManager;

public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<UserDTO> getClazz() {
        return UserDTO.class;
    }

    @Override
    @NotNull
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    @NotNull
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        user.setEmail(email);
        return add(user);
    }

    @Override
    @NotNull
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole((role == null) ? Role.USUAL : role);
        return add(user);
    }

    @Override
    @NotNull
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @NotNull final String lastname,
            @NotNull final String firstName,
            @Nullable final String middleName
    ) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        user.setFirstName(firstName);
        user.setLastName(lastname);
        if (middleName != null) user.setMiddleName(middleName);
        if (email != null) user.setEmail(email);
        return add(user);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

}
