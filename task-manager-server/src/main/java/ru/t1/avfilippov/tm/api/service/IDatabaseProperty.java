package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getSecondLvlCache();

    @NotNull
    String getFactoryClassCache();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinPutsCache();

    @NotNull
    String getRegionPrefixCache();

    @NotNull
    String getCacheConfigFilePath();

}
