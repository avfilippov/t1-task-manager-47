package ru.t1.avfilippov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
